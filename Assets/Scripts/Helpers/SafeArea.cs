﻿using UnityEngine;

public class SafeArea : MonoBehaviour
{
    private RectTransform Panel;
    private Rect LastSafeArea = new Rect(0, 0, 0, 0);

    private void Awake()
    {
        Panel = GetComponent<RectTransform>();
        Refresh();
    }

    private void Update() => Refresh();

    private void Refresh()
    {
        Rect safeArea = GetSafeArea();

        if (safeArea != LastSafeArea)
            ApplySafeArea(safeArea);
    }

    private Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    private void ApplySafeArea(Rect r)
    {
        LastSafeArea = r;

        var anchorMin = r.position;
        var anchorMax = r.position + r.size;

        anchorMin.x /= Screen.width;
        anchorMin.y /= Screen.height;
        anchorMax.x /= Screen.width;
        anchorMax.y /= Screen.height;
        Panel.anchorMin = anchorMin;
        Panel.anchorMax = anchorMax;
    }
}
