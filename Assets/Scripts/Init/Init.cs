﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Init : MonoBehaviour
{
    [SerializeField] private float _timeToLoadNewScene = 3f;

    private bool _isFakeLoadEnded;

    private void Start()
    {
        StartCoroutine(Delay());

        StartCoroutine(LoadScene());
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(_timeToLoadNewScene);

        _isFakeLoadEnded = true;
    }

    private IEnumerator LoadScene()
    {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Main");

        asyncOperation.allowSceneActivation = false;

        while (!asyncOperation.isDone)
        {
            if (asyncOperation.progress >= 0.9f)
            {
                if (_isFakeLoadEnded)
                    asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }

}

