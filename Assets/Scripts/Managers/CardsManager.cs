﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CardsManager : Singleton<CardsManager>
{
    public  Action<Card> OnEndMove;

    public void CreateCard(Card card, Transform cardHolder, Transform dilerPosition, float durationTime)
    {
        Card newCard = Instantiate(card, dilerPosition.position, Quaternion.identity, dilerPosition);

        MoveCardToCardHolder(newCard, cardHolder, dilerPosition, durationTime);
    }

    public void MoveCardToTable(Card card, Transform dilerPosition, float durationTime, float cardLifeCycle)
    {
        card.transform.SetParent(dilerPosition);
        card.transform.DOLocalMove(Vector3.zero, durationTime).OnComplete(() =>
        {
            StartCoroutine(DestroyCard(card, cardLifeCycle));
        });
    }

    private void MoveCardToCardHolder(Card card, Transform cardHolder, Transform dilerPosition, float durationTime)
    {
        card.transform.SetParent(dilerPosition);
        card.transform.DOLocalMove(new Vector3(0f, -400, 0f), durationTime).OnComplete(() =>
        {
            card.transform.SetParent(cardHolder);
        });
    }

    private IEnumerator DestroyCard(Card card, float timeToDestroy)
    {
        yield return new WaitForSeconds(timeToDestroy);

        Destroy(card.gameObject);
    }
   
}