﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TableManager : MonoBehaviour
{

    public static Action<int> OnChangeScore;

    #region SerializeField

    [SerializeField] private Card[] _cardPrefabs;
    [SerializeField] private Transform[] _cardHolders;
    [SerializeField] private Image _dilerPosition;

    [SerializeField] private CardType _currentCardMoveType;
    [SerializeField] private Color _inectiveColor;
    [SerializeField] private Vector2 _minCardSize;
    [SerializeField] private Vector2 _maxCardSize;

    [SerializeField] private int _maxCard = 13;
    [SerializeField] private float _cardLifeCycle = 2f;
    [SerializeField] private float _deleyInDestribution = 0.2f;
    [SerializeField] private float _durationTime = 0.6f;

    #endregion

    private int _score;

    private void OnEnable()
    {
        Card.OnClick += Move;
        CardsManager.Instance.OnEndMove += EndMove;
    }

    private void OnDisable()
    {
        Card.OnClick -= Move;
        CardsManager.Instance.OnEndMove += EndMove;
    }

    private IEnumerator Start()
    { 
        yield return new WaitForSeconds(_durationTime);

        for (var i = 0; i < _maxCard; i++)
        {
            var newCard = _cardPrefabs[Random.Range(0, _cardPrefabs.Length)];
            
            CardsManager.Instance.CreateCard(newCard,_cardHolders[(int)newCard.CardType], transform,_durationTime);
  
            yield return new WaitForSeconds(_deleyInDestribution);
        }

        CheackCardHolderContent();
        _dilerPosition.enabled = false;

        yield return new WaitForSeconds(_durationTime);

        GetNewMove();
    }

    private void EndMove(Card card)
    {
        _score++;
        GetNewMove();
        OnChangeScore.Invoke(_score);
    }

    private void Move(Card card)
    {
        if (card.CardType != _currentCardMoveType)
            return;

        CardsManager.Instance.MoveCardToTable(card, _dilerPosition.transform, _durationTime, _cardLifeCycle);
        EndMove(card);
        CheackCardHolderContent();
        ShowAvailbleCard();
    }

    private void GetNewMove()
    {
        _currentCardMoveType = (CardType)Random.Range(0, _cardPrefabs.Length);

        ShowAvailbleCard();
    }

    private void ShowAvailbleCard()
    {
        for (var i = 0; i < _cardHolders.Length; i++)
        {
            foreach (Transform card in _cardHolders[i])
            {
                card.GetComponent<RectTransform>().sizeDelta = (int)_currentCardMoveType == i ? _maxCardSize : _minCardSize;
                card.GetComponent<Image>().color = (int)_currentCardMoveType == i ? Color.white : _inectiveColor;
            }
        }
    }

    private void CheackCardHolderContent()
    {
        foreach (Transform suit in _cardHolders)
            suit.gameObject.SetActive(!(suit.childCount == 0));
    }

}