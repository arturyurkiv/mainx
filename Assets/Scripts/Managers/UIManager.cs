﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private Button _customizeButton;
    [SerializeField] private GameObject _castomizePanel;
    [SerializeField] private Settings _settings;

    private bool _isCastomizePanelOpen = false;

    private void OnEnable()
    {
       TableManager.OnChangeScore += ChangeScoreUI;
       ClickbleTextureItem.OnBgChanged += ChangeBG;
        _customizeButton.onClick.AddListener(OpenCustomizePanel);
    }

    private void ChangeBG()
    {
        Camera.main.backgroundColor = _settings.BGColor;
    }

    private void OnDisable()
    {
        TableManager.OnChangeScore -= ChangeScoreUI;
    }

    private void Start()
    {  
        ChangeBG();
    }

    private void OpenCustomizePanel()
    {
        _castomizePanel.SetActive(!_isCastomizePanelOpen);
        _isCastomizePanelOpen = !_isCastomizePanelOpen;
    }



    private void ChangeScoreUI(int score) => _scoreText.text = $"{score}";

}

