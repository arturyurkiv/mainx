﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class ClickbleTextureItem : MonoBehaviour, IPointerClickHandler
{
    public static Action OnBgChanged;

    [SerializeField] private Settings _settings;

    public void OnPointerClick(PointerEventData eventData)
    {
        _settings.BGColor = GetComponent<Image>().color;
        OnBgChanged?.Invoke();
    }
}
