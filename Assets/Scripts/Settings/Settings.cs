﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "ScriptableObjects/SettingsScriptableObject", order = 1)]
public class Settings : ScriptableObject
{
    public Color BGColor = Color.blue;


}
