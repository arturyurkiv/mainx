﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;

public class Card : MonoBehaviour, IPointerClickHandler
{
    public static Action<Card> OnClick;

    public CardType CardType
    {
        get => _cardType;
    }

    [SerializeField] private CardType _cardType;

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick?.Invoke(this);
    }

}
